# reddit-saved

 A project borne out of a desperate need to manage my personal content on reddit

## Project setup

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm run serve
```

### Run Storybook for development

```bash
npm run storybook:serve
```

### Compiles and minifies for production

```bash
npm run build
```

### Build Storybook for production

```bash
npm run storybook:build
```

### Lints and fixes files

```bash
npm run lint
```

### Run your unit tests

```bash
npm run test:unit
```
