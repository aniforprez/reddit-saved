// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import {
	Button,
	ButtonGroup,
	Checkbox,
	Input,
	Select,
	Option,
	Row,
	Col,
	Container,
	Header,
	Main,
	Footer,
	Loading,
	Pagination,
	Tag,
	Tooltip
} from 'element-ui';
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition';
// FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faPlusCircle,
	faMinusCircle,
	faChevronUp,
	faChevronDown,
	faLock,
	faBookmark as fasBookmark,
	faCommentAlt,
	faSearch,
	faUserCircle,
	faEnvelopeOpenText,
	faStopwatch,
	faSpinner
} from '@fortawesome/free-solid-svg-icons';
import {
	faRedditAlien,
	faRedditSquare,
	faGithubAlt,
	faGitlab,
	faTwitter,
	faLinkedinIn
} from '@fortawesome/free-brands-svg-icons';
import {
	faEye,
	faEyeSlash,
	faBookmark as farBookmark
} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';

// CSS
import 'element-ui/lib/theme-chalk/index.css';

// Scaffolding the app
import App from './App';
import router from './router';
import store from './store';
import './registerServiceWorker';

Vue.config.productionTip = false;

// Configuring element-ui language
locale.use(lang);

// Form components
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Checkbox);
Vue.use(Input);
Vue.use(Select);
Vue.use(Option);

// Layout elements
Vue.use(Row);
Vue.use(Col);
Vue.use(Container);
Vue.use(Header);
Vue.use(Main);
Vue.use(Footer);

// Miscellaneous
Vue.use(Loading);
Vue.use(Pagination);
Vue.use(Tag);
Vue.use(Tooltip);

Vue.component(CollapseTransition.name, CollapseTransition);

// Loading fontAwesome icons
library.add(
	faPlusCircle,
	faMinusCircle,
	faChevronUp,
	faChevronDown,
	faLock,
	fasBookmark,
	farBookmark,
	faCommentAlt,
	faSearch,
	faUserCircle,
	faStopwatch,
	faSpinner,
	faRedditAlien,
	faRedditSquare,
	faEnvelopeOpenText,
	faTwitter,
	faGithubAlt,
	faGitlab,
	faLinkedinIn,
	faEye,
	faEyeSlash
);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
