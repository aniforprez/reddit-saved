// Name this file config.dev.js or config.prod.js depending on your env
module.exports = {
	clientId: 'reddit-app-clientid',
	redirectUri: 'http://localhost:8080/token',
	scope: ['history', 'save', 'identity', 'vote', 'report'],
	permanent: false
};
