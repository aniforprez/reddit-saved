function convertListingToList(contentListing) {
	let listingJSON = contentListing.toJSON();
	let list = listingJSON.map(listItem => {
		let item = {
			id: listItem.id,
			name: listItem.name,
			title: listItem.title || listItem.link_title,
			url: listItem.url,
			domain: listItem.domain,
			subreddit: listItem.subreddit,
			subreddit_id: listItem.subreddit_id,
			subreddit_name: listItem.subreddit_name_prefixed,
			permalink: listItem.permalink,
			likes: listItem.likes,
			thumbnail: listItem.thumbnail || null,
			thumbnail_dimensions: {
				height: listItem.thumbnail_height || null,
				width: listItem.thumbnail_width || null
			},
			body: listItem.body || null,
			body_html: listItem.body_html || null,
			selftext: listItem.selftext || null,
			selftext_html: listItem.selftext_html || null,
			author: listItem.author,
			score: listItem.score,
			hide_score: listItem.hide_score || false,
			num_comments: listItem.num_comments,
			created: listItem.created,
			saved: listItem.saved,
			gilded: listItem.gilded,
			hidden: listItem.hidden || false,
			nsfw: listItem.over_18,
			archived: listItem.archived,
			is_comment: !!listItem.link_title,
			all_comments_link: listItem.link_permalink,
			link_url: listItem.link_url || null
		};
		if (
			listItem.thumbnail === 'self' ||
			listItem.thumbnail === 'default' ||
			listItem.thumbnail === 'image' ||
			item.thumbnail_dimensions.height === null ||
			item.thumbnail_dimensions.width === null
		) {
			item.thumbnail = null;
		}
		return item;
	});

	return list;
}

function getUniqueSubreddits(contentList) {
	return contentList
		.reduce((subredditList, savedItem) => {
			let itemIndex = subredditList.findIndex(
				el => el.subreddit === savedItem.subreddit
			);
			if (itemIndex < 0) {
				subredditList.push({
					subreddit: savedItem.subreddit,
					count: 1
				});
			} else {
				subredditList[itemIndex].count++;
			}

			return subredditList;
		}, [])
		.sort((a, b) =>
			a.subreddit.toLowerCase().localeCompare(b.subreddit.toLowerCase())
		);
}

export { convertListingToList, getUniqueSubreddits };
