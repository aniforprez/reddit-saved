import snoowrap from 'snoowrap';

import { convertListingToList, getUniqueSubreddits } from './common_methods';

const state = {
	filter: {
		text: '',
		nsfw: null,
		subreddits: []
	},
	errorLoadingSaved: false,
	loadingSaved: false,
	loadedSaved: false,
	savedList: [],
	savedSubreddits: []
};

const getters = {
	filter: state => state.filter,
	loadedSaved: state => state.loadedSaved,
	loadingSaved: state => state.loadingSaved,
	totalSavedCount: state => state.savedList.length,
	savedSubreddits: state => state.savedSubreddits,
	filteredSavedList: state => {
		return state.savedList.filter(savedItem => {
			// TODO: Add datewise search
			let conditions = [];
			if (state.filter.text && state.filter.text.length >= 2) {
				// TODO: Improve textsearch to search other potential fields
				let textSearch = [];
				textSearch.push(
					savedItem.title
						.toLowerCase()
						.indexOf(state.filter.text.toLowerCase()) > -1
				);
				conditions.push(textSearch.some(val => val));
			}
			if (state.filter.subreddits.length > 0) {
				conditions.push(
					state.filter.subreddits.indexOf(savedItem.subreddit) > -1
				);
			}
			if (state.filter.nsfw) {
				conditions.push(savedItem.nsfw);
			}
			if (conditions.every(val => val)) {
				return true;
			}
		});
	}
};

const mutations = {
	loadingSaved(state) {
		state.savedList = [];
		state.loadingSaved = true;
	},
	fetchedPartialSavedList(state, { savedList }) {
		state.savedList = Object.freeze(savedList);
		state.savedSubreddits = Object.freeze(
			getUniqueSubreddits(state.savedList)
		);
	},
	successFetchingSavedList(state, { savedList }) {
		state.savedList = Object.freeze(savedList);
		state.savedSubreddits = Object.freeze(
			getUniqueSubreddits(state.savedList)
		);
		state.loadingSaved = false;
		state.loadedSaved = true;
		state.errorLoadingSaved = false;
	},
	failureFetchingSavedList(state) {
		state.savedList = [];
		state.loadingSaved = false;
		state.errorLoadingSaved = true;
	},
	votePost(state, { index, action }) {
		if (action === 'upvote') {
			state.savedList[index].score +=
				state.savedList[index].likes === false ? 2 : 1;
			state.savedList[index].likes = true;
		}
		if (action === 'downvote') {
			state.savedList[index].score -= state.savedList[index].likes
				? 2
				: 1;
			state.savedList[index].likes = false;
		}
		if (action === 'unvote') {
			if (state.savedList[index].likes) {
				state.savedList[index].score -= 1;
			}
			if (state.savedList[index].likes === false) {
				state.savedList[index].score += 1;
			}
			state.savedList[index].likes = null;
		}
	},
	setSavedPost(state, { index, saved }) {
		state.savedList[index].saved = saved;
	},
	setHiddenPost(state, { index, hidden }) {
		state.savedList[index].hidden = hidden;
	},
	setFilterNSFW(state, nsfw) {
		state.filter.nsfw = nsfw;
	},
	setFilterSubreddits(state, subreddits) {
		state.filter.subreddits = subreddits;
	},
	setFilterText(state, text) {
		state.filter.text = text;
	}
};

const actions = {
	async loadSavedList({ commit, rootState }) {
		let r = new snoowrap({ accessToken: rootState.auth.authToken });

		commit('loadingSaved');
		let savedListing = null;
		try {
			savedListing = await r.getMe().getSavedContent();
		} catch (error) {
			commit('failureFetchingSavedList');
			throw error;
		}
		while(!savedListing.isFinished) {
			let savedList = convertListingToList(savedListing);
			commit('fetchedPartialSavedList', { savedList });
			try {
				savedListing = await savedListing.fetchMore({
					amount: 100
				});
			} catch (error) {
				commit('failureFetchingSavedList');
				throw error;
			}
		}
		let savedList = convertListingToList(savedListing);
		commit('successFetchingSavedList', { savedList });
	},
	async votePost({ commit, state, rootState }, { postId, action }) {
		let r = new snoowrap({ accessToken: rootState.auth.authToken });
		let postIndex = state.savedList.findIndex(item => {
			return item.id === postId;
		});

		if (
			(action === 'upvote' && state.savedList[postIndex].likes) ||
			(action === 'downvote' &&
				state.savedList[postIndex].likes === false)
		) {
			action = 'unvote';
		}

		let voteContent = null;
		try {
			if (state.savedList[postIndex].is_comment) {
				voteContent = await r.getComment(postId);
			} else {
				voteContent = await r.getSubmission(postId);
			}

			if (action === 'upvote') {
				await voteContent.upvote();
			}
			if (action === 'downvote') {
				await voteContent.downvote();
			}
			if (action === 'unvote') {
				await voteContent.unvote();
			}
		} catch (error) {
			throw error;
		}
		commit('votePost', { index: postIndex, action });
	},
	async toggleSavePost({ commit, state, rootState }, { postId }) {
		let r = new snoowrap({ accessToken: rootState.auth.authToken });
		let postIndex = state.savedList.findIndex(item => {
			return item.id === postId;
		});

		let saveContent = null;
		try {
			if (state.savedList[postIndex].is_comment) {
				saveContent = await r.getComment(postId);
			} else {
				saveContent = await r.getSubmission(postId);
			}

			if (state.savedList[postIndex].saved) {
				await saveContent.unsave();
			} else {
				await saveContent.save();
			}
		} catch (error) {
			throw error;
		}

		commit('setSavedPost', {
			index: postIndex,
			saved: !state.savedList[postIndex].saved
		});
	},
	async toggleHidePost({ commit, state, rootState }, { postId }) {
		let r = new snoowrap({ accessToken: rootState.auth.authToken });
		let postIndex = state.savedList.findIndex(item => {
			return item.id === postId;
		});
		if (state.savedList[postIndex].is_comment) {
			throw new Error('Cannot hide comment');
		}

		try {
			if (state.savedList[postIndex].hidden) {
				await r.getSubmission(postId).unhide();
			} else {
				await r.getSubmission(postId).hide();
			}
		} catch (error) {
			throw error;
		}

		commit('setHiddenPost', {
			index: postIndex,
			hidden: !state.savedList[postIndex].hidden
		});
	}
};

export default {
	state,
	getters,
	mutations,
	actions
};
