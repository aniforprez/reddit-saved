import snoowrap from 'snoowrap';

import config from '@/config';

const state = {
	authToken: null,
	authError: false,
	authExpireTime: 0,
	username: ''
};

const getters = {
	redirectUrl: () => {
		return snoowrap.getAuthUrl({
			clientId: config.clientId,
			scope: config.scope,
			redirectUri: config.redirectUri,
			permanent: config.permanent
		});
	},
	authToken: state => state.authToken,
	authorized: state => !!state.authToken,
	username: state => state.username,
	authExpireTime: state => state.authExpireTime
};

const mutations = {
	authSuccess(state, { authToken, authTime }) {
		state.authToken = authToken;
		state.authError = false;

		if (authTime) {
			localStorage.setItem('authToken', authToken);
			localStorage.setItem('authTime', authTime);

			state.authExpireTime = authTime + 60 * 60 * 1000;
		} else {
			state.authExpireTime =
				parseInt(localStorage.getItem('authTime')) + 60 * 60 * 1000;
		}
	},
	authFailure(state) {
		state.authToken = null;
		state.authError = true;
	},
	authExpired(state) {
		state.authToken = null;

		localStorage.removeItem('authToken');
		localStorage.removeItem('authTime');
	},
	setUsername(state, { username }) {
		state.username = username;
	}
};

const actions = {
	async checkAlreadyAuthorized({ state, commit }) {
		const authToken = localStorage.getItem('authToken');
		const authTime = localStorage.getItem('authTime');

		if (authToken && !state.authToken) {
			if (Date.now() - authTime > 60 * 60 * 1000) {
				localStorage.removeItem('authToken');
				localStorage.removeItem('authTime');
				return;
			}

			setTimeout(() => {
				commit('authExpired');
			}, 60 * 60 * 1000 - (Date.now() - authTime));

			commit('authSuccess', { authToken });

			let r = new snoowrap({ accessToken: authToken });
			let userInfo = await r.getMe().fetch();
			commit('setUsername', { username: userInfo.name });
		}
	},
	async authorize({ commit }, code) {
		let instance = null;
		try {
			instance = await snoowrap.fromAuthCode({
				code,
				clientId: config.clientId,
				redirectUri: config.redirectUri
			});
		} catch (error) {
			commit('authFailure');
			throw error;
		}
		const authToken = instance.accessToken;

		commit('authSuccess', { authToken, authTime: Date.now() });

		let r = new snoowrap({ accessToken: authToken });
		let userInfo = await r.getMe().fetch();
		commit('setUsername', { username: userInfo.name });
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};
